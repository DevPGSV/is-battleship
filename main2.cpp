#include <SFML/Graphics.hpp>

#include "ResourceManager.h"
int main2()
{
	return 0;
	sf::RenderWindow window(sf::VideoMode(600, 800), "SFML works!");
	sf::CircleShape shape(100.f);
	shape.setFillColor(sf::Color::Green);

	sf::Sprite Test1, Test2, Test3, Test4, Test5, Test6;

	Test1.setTexture(*ResourceManager::GetInstance()->GetTexture("TestImage.png"));
	Test1.setPosition(sf::Vector2f(0, 50));

	Test2.setTexture(*ResourceManager::GetInstance()->GetTexture("TestImage.png"));
	Test2.setPosition(sf::Vector2f(0, 100));
	
	Test3.setTexture(*ResourceManager::GetInstance()->GetTexture("TestImage.png"));
	Test3.setPosition(sf::Vector2f(0, 200));

	Test4.setTexture(*ResourceManager::GetInstance()->GetTexture("TestImage.png"));
	Test4.setPosition(sf::Vector2f(0, 300));

	Test5.setTexture(*ResourceManager::GetInstance()->GetTexture("TestImage.png"));
	Test5.setPosition(sf::Vector2f(0, 400));

	Test6.setTexture(*ResourceManager::GetInstance()->GetTexture("TestImage.png"));
	Test6.setPosition(sf::Vector2f(0, 500));

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear();
		window.draw(shape);
		window.draw(Test1);
		window.draw(Test2);
		window.draw(Test3);
		window.draw(Test4);
		window.draw(Test5);
		window.draw(Test6);
		window.display();
	}

	return 0;
}