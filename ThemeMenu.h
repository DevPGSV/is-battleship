#pragma once
#include "cScreen.h"
#include <string>
#include <list>
#include <map>
#include <SFML/Graphics.hpp>

class ThemeMenu : public cScreen {
private:
	typedef struct {
		sf::Text text;
		bool isSelected;
		int returnValue;
		std::string mapKey;
		std::string mapValue;
	} menuEntry;
	std::vector<menuEntry> menuEntryList;
	void StartGame();
	sf::Sprite title;
	sf::Font *Font;
	int menuSelected;
	menuEntry createEntry(std::string str, float positionX, float positionY, int returnValue, int charSize, std::string mapKey, std::string mapValue);
public:
	ThemeMenu();
	virtual int Run(sf::RenderWindow &window, std::map<std::string, std::string> &dataStr);
};
