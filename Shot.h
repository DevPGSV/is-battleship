
#ifndef SHOT_H
#define SHOT_H
#include "GameObject.h"

#include <string>
#include "Coordinate.h"
#include "SFML/Graphics/Sprite.hpp"

enum ShotType
{
	Null, BoatShot, WaterShot, Other
};
class Shot : virtual public GameObject
{
public:

	Shot();

	virtual ~Shot();

	bool InitShot(ShotType Type, Coordinate Pos, sf::Vector2f boardI);

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	Coordinate getPosition();
	ShotType getShotType();
private:
	Coordinate Position;
	sf::Sprite sprite;
	ShotType shotType;
	Coordinate coordinates;
};


#endif // SHOT_H
