
#ifndef GAMEBOARD_H
#define GAMEBOARD_H
#include "GameObject.h"
#include <vector>
#include "Boat.h"
#include "Shot.h"
#include "Coordinate.h"
#include "SpriteAnimated.h"

enum CellType
{
	Water_Cell, Boat_Cell, Other_Cell, OutOfBoard_Cell
};
class GameBoard : virtual public GameObject
{
public:
  GameBoard ( );
  virtual ~GameBoard ( );
  virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
  virtual bool LoadAssets();
  void setShot(Coordinate coord);
  ShotType getShot(Coordinate coord);
  bool setBoat(Coordinate coord, BoatType type, Direction dir);
  CellType getCell(Coordinate coord);
  bool checkIfBoat(Coordinate coord);
  sf::Sprite Background;
  SpriteAnimated BackgroundAnim;
private:
	std::vector<Boat> BoatArray;
	std::vector<Shot> ShotArray;


};

#endif // GAMEBOARD_H