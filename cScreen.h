#pragma once
#include <SFML/Graphics/RenderWindow.hpp>
#include <map>
#include <string>

class cScreen {
public :
    virtual int Run (sf::RenderWindow &window, std::map<std::string, std::string> &dataStr) = 0;
};
