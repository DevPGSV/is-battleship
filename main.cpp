#include <iostream>
#include <SFML/Graphics.hpp>
#include <map>
#include <string>
#include "cScreen.h"

//Windows to display:
#include "MainMenu.h"
#include "Game.h"
#include "ThemeMenu.h"

const int WINDOW_FPS = 60;

int main () {
	//Prepare

	//Create window
	sf::RenderWindow window(sf::VideoMode(1300, 800), sf::String("BattleShip!"));
	window.setFramerateLimit(WINDOW_FPS); //60fps
	const float DeltaTime = 1 / WINDOW_FPS;

	
	//Screen vector:
	std::vector<cScreen*> Screens;
    int screen = 0;

	//Add screens to vector:
	MainMenu s0;
    Screens.push_back(&s0);
	Game s1;
    Screens.push_back(&s1);
	ThemeMenu s2;
	Screens.push_back(&s2);

	//Config:
	std::map<std::string, std::string> dataStr;
	dataStr["theme"] = "default";
	
	while (screen >= 0) { // -1 to exit
		screen = Screens[screen]->Run(window, dataStr);
    }
	
	return EXIT_SUCCESS;
}