
#ifndef PLAYER_H
#define PLAYER_H

#include "SFML/Window/Event.hpp"
enum PlayerAction
{
	MoveUp,MoveDown,MoveLeft,MoveRight,Fire,Nothing,Place
};

class Player
{
public:

	Player();


	virtual ~Player();




	virtual PlayerAction CheckAction(sf::Event Event);;

};

#endif // PLAYER_H
