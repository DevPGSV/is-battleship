#include <SFML/Graphics.hpp>
#include "SpriteAnimated.h"

void SpriteAnimated::set(sf::Texture &texture, int h, int w, int frames, int framesPerSecond) {
	sprite = sf::Sprite();
	height = h;
	width = w;
	currentFrame = 0;
	MaxFrames = frames;
	fps = framesPerSecond;
	sprite.setTexture(texture);
	setFrame(currentFrame);
}

SpriteAnimated::SpriteAnimated() { }

SpriteAnimated::~SpriteAnimated () { }

sf::Sprite* SpriteAnimated::getSpritePtr(){
	return &sprite;
}


void SpriteAnimated::setFrame(int f) {
	currentFrame = f;
	sprite.setTextureRect(sf::IntRect(currentFrame * width, 0, width, height));
}

void SpriteAnimated::Update() {
	if (clock.getElapsedTime().asMilliseconds() >= (1000/fps)) {
		if (currentFrame == MaxFrames) {
			setFrame(0);
		} else {
			setFrame(currentFrame+1);
		}
		clock.restart();
	}
}


/*
    /// <summary>
    /// Create new SpriteAnimated.
    /// </summary>
    /// <param name="Text">Texture to use in your sprite.</param>
    /// <param name="FrameWidth">Width of one frame in pixels.</param>
    /// <param name="FrameHeight">Height of one frame in pixels.</param>
    /// <param name="FramesPerSecond">Your sprite's FPS.</param>
    /// <param name="RTarget">RenderTarget reference.</param>
    /// <param name="RStates">RenderStates object.</param>
    /// <param name="FirstFrame">First frame of animation sequence.</param>
    /// <param name="LastFrame">Last frame of animation sequence.</param>
    /// <param name="IsAnimated">Should sequence be played immediately after creation? If false, first frame will be paused.</param>
    /// <param name="IsLooped">Should sequence be looped? If false, animation will stop after one full sequence.</param>
    SpriteAnimated::SpriteAnimated(sf::Texture Text, int FrameWidth, int FrameHeight, sf::RenderStates RStates, int FirstFrame,  int LastFrame, bool IsAnimated, bool IsLooped) {
		//_renderTarget = RTarget;
        _renderStates = RStates;
        _frameWidth = FrameWidth;
        _frameHeight = FrameHeight;
        _frameCount = LastFrame - FirstFrame;
        _firstFrame = FirstFrame;
        _currentFrame = FirstFrame;
        _lastFrame = LastFrame;
        _isAnimated = IsAnimated;
        _isLooped = IsLooped;
		sprite.setTexture(Text);
		sprite.setTextureRect(GetFramePosition(_currentFrame));
    }
	
    /// <summary>
    /// This method calculates TextureRect coordinates for a certain frame.
    /// </summary>
    /// <param name="frame">Frame which coordinates you need.</param>
    /// <returns>Returns frame coordinates as IntRect.</returns>
    sf::IntRect SpriteAnimated::GetFramePosition(int frame) {
		int WCount = (int)sprite.getTexture()->getSize().x / _frameWidth;
        int XPos = frame % WCount;
        int YPos = frame / WCount;
        sf::IntRect Position = sf::IntRect(_frameWidth * XPos, _frameHeight * YPos, _frameWidth, _frameHeight);
        return Position;
    }

    /// <summary>
    /// This method is used to update animation state and draw sprite.
    /// You should avoid using Draw() method if you use this.
    /// </summary>
    void SpriteAnimated::Update() {
        if (_isAnimated) {
			sprite.setTextureRect(GetFramePosition(_currentFrame));
            if (_currentFrame < _lastFrame) {
                _currentFrame++;
			} else {
                _currentFrame = _firstFrame;
			}
        }

        if (!_isLooped & (_currentFrame == _lastFrame))
        {
             _isAnimated = false;
        }

        //sprite.draw(_renderTarget, _renderStates);
    }

    /// <summary>
    /// Resume animation with current settings.
    /// </summary>
    void SpriteAnimated::Play()
    {
        _isAnimated = true;
    }

    /// <summary>
    /// Pause current animation.
    /// </summary>
    void SpriteAnimated::Pause()
    {
        _isAnimated = false;
    }

    /// <summary>
    /// Stop animation and return to frame zero.
    /// </summary>
	void SpriteAnimated::Reset()
    {
        _isAnimated = false;
        _currentFrame = 0;
        sprite.setTextureRect(sf::IntRect(0, 0, _frameWidth, _frameHeight));
    }

    /// <summary>
    /// Set new animation sequence.
    /// </summary>
    /// <param name="FirstFrame">First frame of new sequence.</param>
    /// <param name="LastFrame">Last frame of new sequence.</param>
    /// <param name="IsAnimated">Should sequence be played immediately? If false, first sequence frame will be paused.</param>
    /// <param name="IsLooped">Should sequence be looped? If false, animation will stop after one full sequence.</param>
    void SpriteAnimated::SetAnimation(int FirstFrame, int LastFrame, bool IsAnimated, bool IsLooped)
    {
        _firstFrame = FirstFrame;
        _lastFrame = LastFrame;
        _isAnimated = IsAnimated;
        _isLooped = IsLooped;

        _frameCount = (LastFrame + 1) - FirstFrame;
        if (!IsAnimated)
        {
             sprite.setTextureRect(GetFramePosition(FirstFrame));
        }
    }
    /// <summary>
    /// Pause on particular frame.
    /// </summary>
    /// <param name="Frame">Frame number.</param>
    void SpriteAnimated::SetFrame(int Frame)
    {
        _currentFrame = Frame;
        _isAnimated = true;
        _isLooped = false;
    }
	
*/