# README #


Battleship game for IS subject. 
Access the code documentation [HERE](https://bitbucket.org/DevPGSV/is-battleship/wiki/Design) for implementation details

### Work to do ###

* Create use cases // están en presentación
* Complete the program from the use cases

### Current state ###
(07/11/0214)
Launch in Main Menu. In the game: the board, ships, target and shots are displayed.

### Distribution of work ###

* Each one can have a module to code, or several if they are closely related.
* [Issue traking](https://bitbucket.org/DevPGSV/is-battleship/issues)

### Wiki ###

* Content about the proyect:
* [Go to wiki](https://bitbucket.org/DevPGSV/is-battleship/wiki/Home)

### Progress ###

Progress can be seen in the screenshots folder. Also, from time to time it will be updated in [Progress](https://bitbucket.org/DevPGSV/is-battleship/wiki/progress/Home)