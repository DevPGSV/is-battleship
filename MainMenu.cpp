#include <iostream>
#include <SFML/Window/Keyboard.hpp>
#include "MainMenu.h"
#include "ResourceManager.h"

MainMenu::MainMenu() {
	//Preparation
	std::cout << "MainMenu -> Constructor" << std::endl;

	StartGame();
}

void MainMenu::StartGame() {
	//First time execution
	std::cout << "MainMenu -> StartGame" << std::endl;
	menuSelected = 0;

	Font = ResourceManager::GetInstance()->GetFont("RESOURCES/font/sansation.ttf");	

	if (Font == nullptr) {
		std::cout << "Error loading 'RESOURCES/font/sansation.ttf'" << std::endl;
        return;
    }

	//Font = *fontPtr;

	title.setTexture(*ResourceManager::GetInstance()->GetTexture("RESOURCES/img/title/title.png"));
	title.setPosition(sf::Vector2f(1300/2-700/2, -30));
	
	menuEntryList.push_back(createEntry("Play PvP", 550, 150, 1, 40, "mode", "PvP"));
	menuEntryList.push_back(createEntry("Play CPU", 550, 250, 1, 40, "mode", "CPU"));
	menuEntryList.push_back(createEntry("Play Online", 550, 350, 1, 40, "mode", "Online"));
	menuEntryList.push_back(createEntry("Choose theme", 550, 450, 2, 40, "", ""));
	menuEntryList.push_back(createEntry("Exit", 550, 550, -1, 40, "", ""));
	//menuEntryList.push_back(createEntry("Play WTF", 550, 650, 1, 40, "mode", ""));
}

int MainMenu::Run(sf::RenderWindow &window, std::map<std::string, std::string> &dataStr) {
    sf::Event Event;
    bool optSelected = false;

	while (!optSelected)
    {
        //Verifying events
        while (window.pollEvent(Event))
        {
            // Window closed
            if (Event.type == sf::Event::Closed)
            {
                return (-1);
            }
            //Key pressed
            if (Event.type == sf::Event::KeyPressed)
            {
                switch (Event.key.code) {
					case sf::Keyboard::Up:
						menuSelected--;
						break;
					case sf::Keyboard::Down:
						menuSelected++;
						break;
					default:
						break;
				}

				if (menuSelected < 0) menuSelected = menuEntryList.size() - 1;
				if (menuSelected > menuEntryList.size() - 1) menuSelected = 0;

				if ((Event.key.code == sf::Keyboard::Return) || (Event.key.code == sf::Keyboard::Space)){
					optSelected = true;
                }

				if (Event.key.code == sf::Keyboard::Escape) {
					return -1;
                }
            }
        }
		
		for (int i = 0; i < menuEntryList.size(); i++) {
			menuEntryList[i].text.setColor(sf::Color::White);
		}
		menuEntryList[menuSelected].text.setColor(sf::Color::Blue);
		
        //Clearing screen
        window.clear();
        //Drawing
        window.draw(title);
		for (int i = 0; i < menuEntryList.size(); i++) {
			window.draw(menuEntryList[i].text);
		}
        window.display();

    }
	dataStr[menuEntryList[menuSelected].mapKey] = menuEntryList[menuSelected].mapValue;
	return menuEntryList[menuSelected].returnValue;
}

MainMenu::menuEntry MainMenu::createEntry(std::string str, float positionX, float positionY, int returnValue, int charSize, std::string mapKey, std::string mapValue) {
	menuEntry entry;
	entry.isSelected = false;
	entry.returnValue = returnValue;
	if (Font != nullptr)
	{
		entry.text.setFont(*Font);
	}
	entry.text.setCharacterSize(charSize);
    entry.text.setString(str);
	entry.text.setPosition(positionX, positionY);
	entry.mapKey = mapKey;
	entry.mapValue = mapValue;
	return entry;
}
