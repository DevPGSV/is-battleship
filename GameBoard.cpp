#include "GameBoard.h"
#include "SFML/Graphics/Texture.hpp"
#include "ResourceManager.h"
#include "SFML/Graphics/RenderTarget.hpp"
#include <iostream>

GameBoard::GameBoard ( ) {
}

GameBoard::~GameBoard ( ) { }

void GameBoard::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	//Draw Board somehow
	
	target.draw(Background, states);
	//Draw all the boats
	for (Boat boat : BoatArray)
	{
		boat.draw(target, states);
	}

	//Draw all the shots
	for (Shot shot : ShotArray)
	{
		shot.draw(target, states);
	}

}

bool GameBoard::LoadAssets()
{
	Background = sf::Sprite();

	sf::Texture * Tex = ResourceManager::GetInstance()->GetTexture ("RESOURCES/img/water/water_anim.png");
	if (Tex == nullptr)
	{
		//Error Loading the texture
		return false;
	}
	/*
	Background.setTexture(*Tex);
	Tex->setRepeated(true);
	
	//Background.setScale(5, 5);
	Background.setTextureRect(sf::IntRect(0,0,500,500));
	*/


	BackgroundAnim.set(*Tex, 50, 50, 9, 10);

	std::cout << "GameBoard -> LoadAssets: Successful" << std::endl;
	BackgroundAnim.getSpritePtr()->setPosition(0, 0);
	return true;
}

void GameBoard::setShot(Coordinate coord) {
	Shot shot;
	ShotType type;
	if (getCell(coord) == Boat_Cell) {
		type = BoatShot;
	} else if (getCell(coord) == Water_Cell){
		type = WaterShot;
	} else {
		type = Other;
		return;
	}
	shot.InitShot(type, coord, Background.getPosition());
	ShotArray.push_back(shot);
}

ShotType GameBoard::getShot(Coordinate coord) {
	for (int i = 0; i < ShotArray.size(); i++) {
		if (ShotArray[i].getPosition() == coord) {
			return ShotArray[i].getShotType();
		}
	}
	return Null;
}

bool GameBoard::setBoat(Coordinate coord, BoatType type, Direction dir) {
	Boat boat;
	//Cheak if can be placed, else return false
	boat.InitBoat(type, coord, dir, Background.getPosition());
	BoatArray.push_back(boat);
	return true;
}

CellType GameBoard::getCell(Coordinate coord) {
	if (coord.X > 10 || coord.X <= 0 || coord.Y > 10 || coord.Y <= 0)
	{
		return Other_Cell;
	}
	for (auto cell : ShotArray)
	{
		if (cell.getPosition() == coord)
		{
			return Other_Cell;
		}
	}
	if (checkIfBoat(coord)) {
		return Boat_Cell;
	} else {
		return Water_Cell;
	}
}

bool GameBoard::checkIfBoat(Coordinate coord) {
	
	for (auto boat : BoatArray)
	{
		if (boat.CheckShot(coord))
		{
			return true;
		}
	}
	return false;
}