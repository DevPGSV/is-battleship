#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/System/Clock.hpp>

class SpriteAnimated{
private:
	sf::Sprite sprite;
	int height, width, currentFrame, MaxFrames, fps;
	void setFrame(int f);
	sf::Clock clock;
public:
	SpriteAnimated();
	virtual ~SpriteAnimated ( );
	void set(sf::Texture &texture, int h, int w, int frames, int framesPerSecond = 10);
	sf::Sprite* getSpritePtr();
	void Update();

/*
private:
	//sf::RenderTarget &_renderTarget;
	sf::RenderStates _renderStates;
	int _frameWidth, _frameHeight, _frameCount, _currentFrame, _firstFrame, _lastFrame;
	bool _isAnimated;
	bool _isLooped;
	sf::Sprite sprite;
	
public:
	SpriteAnimated(sf::Texture Text, int FrameWidth, int FrameHeight, sf::RenderStates RStates, int FirstFrame = 0,  int LastFrame = 0, bool IsAnimated = false, bool IsLooped = true);
	sf::IntRect GetFramePosition(int frame);
	void Update();
	void Play();
	void Pause();
	void Reset();
	void SetAnimation(int FirstFrame, int LastFrame, bool IsAnimated = true, bool IsLooped = true);
	void SetFrame(int Frame);
*/
};