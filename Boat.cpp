#include "Boat.h"
#include "ResourceManager.h"
#include "SFML/Graphics/RenderTarget.hpp"

// Constructors/Destructors

Boat::Boat () {

	sf::Texture * Tex = ResourceManager::GetInstance()->GetTexture ("RESOURCES/img/boat/$THEME/test.png");
	if (Tex == nullptr)
	{
		//Error Loading the texture
		return;
	}
	sprite.setTexture(*Tex);
}

Boat::~Boat () { }

bool Boat::InitBoat(BoatType Type, Coordinate Pos, Direction Dir, sf::Vector2f boardI)
{
	type = type;
	Position = Pos;
	direction = Dir;
	
	Coordinate extraCoords;
	float angle;
	switch (direction) {
	case Up:
		angle = 180;
		extraCoords.X = 0;
		extraCoords.Y = 0;
		break;
	case Down:
		extraCoords.X = -50;
		extraCoords.Y = -50;
		angle = 0;
		break;
	case Left:
		extraCoords.X = 0;
		extraCoords.Y = -50;
		angle = 90;
		break;
	case Right:
		extraCoords.X = -50;
		extraCoords.Y = 0;
		angle = 270;
		break;
	default:
		extraCoords.X = 0;
		extraCoords.Y = 0;
		angle = 0;
		break;
	}
	coordinates.x = boardI.x + Pos.X * 50 + extraCoords.X;
	coordinates.y = boardI.y + Pos.Y * 50 + extraCoords.Y;
	sprite.setPosition(coordinates.x, coordinates.y);
	sprite.setRotation(angle);
	return false;
}


Coordinate Boat::getPosition() {
	return Position;
}

Direction Boat::getDirection() {
	return direction;
}

int Boat::getSize() {
	switch (type) {
	case Frigate:
		return 3;
		break;
	case Destroyer:
		return 3;
		break;
	case AircraftCarrier:
		return 3;
		break;
	default:
		return 1;
		break;
	}
}

void Boat::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	target.draw(sprite, states);
}

bool Boat::CheckShot(Coordinate Target)
{
	int boatsize = 3;
	Coordinate coord = Target;

	for (int i = 0; i < boatsize; i++)
	{
		
		if (coord == Position)
		{
			return true;
		}
		switch (direction)
		{
		case Up:
			coord.Y++;
			break;
		case Down:
			coord.Y--;
			break;
		case Left:
			coord.X++;
			break;
		case Right:
			coord.X--;
			break;
			
		}
	}
	return false;
}
