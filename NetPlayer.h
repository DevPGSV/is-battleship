
#ifndef NETPLAYER_H
#define NETPLAYER_H
#include "Player.h"


/**
  * class NetPlayer
  * 
  */

class NetPlayer : virtual public Player
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  NetPlayer ( );

  /**
   * Empty Destructor
   */
  virtual ~NetPlayer ( );



};

#endif // NETPLAYER_H
