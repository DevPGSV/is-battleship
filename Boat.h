
#ifndef BOAT_H
#define BOAT_H
#include "GameObject.h"

#include <string>
#include "Coordinate.h"
#include "SFML/Graphics/Sprite.hpp"


enum Direction
{
	Up,Right,Down,Left,None
};

enum BoatType
{
	Frigate, Destroyer, AircraftCarrier,NoType
};

class Boat : public GameObject
{
public:

  Boat ( );

  virtual ~Boat ( );

  bool CheckShot(Coordinate Target);

  bool InitBoat(BoatType Type, Coordinate Pos, Direction Dir, sf::Vector2f boardI);  
  Coordinate getPosition();
  Direction getDirection();
  int getSize();
  virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;


private:
	sf::Vector2f coordinates;
	sf::Sprite sprite;
	Coordinate Position;
	Direction direction;
	BoatType type;
};

#endif // BOAT_H
