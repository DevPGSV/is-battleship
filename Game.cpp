#include <iostream>
#include <time.h>
#include <sstream>
#include "Game.h"
#include "HumanPlayer.h"
#include "AIPlayer.h"
#include "NetPlayer.h"
#include "ResourceManager.h"

Game::Game() {
	//Preparation
	std::cout << "Game -> Constructor" << std::endl;
}

void Game::StartGame() {
	BoatsPlaced = 4;
	//First time execution
	// (create objects, things, variables, whatever)
	if (!Font.loadFromFile("RESOURCES/font/sansation.ttf")) {
		std::cout << "Error loading 'RESOURCES/font/sansation.ttf'" << std::endl;
        return;
    }
	
	displayText.setString("Welcome!");
	displayText.setFont(Font);
	displayText.setPosition(5, 800-25);
	displayText.setCharacterSize(20);

	StateText.setString("");
	StateText.setFont(Font);
	StateText.setPosition(5, 5);
	StateText.setCharacterSize(20);

	std::cout << "Game -> StartGame" << std::endl;
	SetState(PreGame);

	Board1 = GameBoard();
	Board1.LoadAssets();

	Board1.Background.setPosition(100, 400 - 250);
	Board2 = GameBoard();
	Board2.LoadAssets();
	Board2.Background.setPosition(1300-600 , 400 - 250);

	ShotTarget.setTexture(*ResourceManager::GetInstance()->GetTexture("RESOURCES/img/shotTarget/shotTarget.png"));
	ShotTargetCoord.X = 1;
	ShotTargetCoord.Y = 1;

	//players
	Player1 = new HumanPlayer();
	CurrentTurn = 0;

	BoatToPlace = Frigate;
	BoatDirection = Down;
}

int Game::Run(sf::RenderWindow &window, std::map<std::string, std::string> &dataStr) {
	bool run = true;
	ResourceManager::GetInstance()->setTheme(dataStr["theme"]);

	StartGame();
	if (dataStr["mode"] == "PvP") {
		Player2 = new HumanPlayer();
		std::cout << "Play -> Run: " << "PvP mode" << std::endl;
	} else if (dataStr["mode"] == "CPU") {
		Player2 = new AIPlayer();
		std::cout << "Play -> Run: " << "PvCPU Mode" << std::endl;
	} else if (dataStr["mode"] == "Online") {
		Player2 = new NetPlayer();
		std::cout << "Play -> Run: " << "PvP Online Mode" << std::endl;
	} else {
		std::cout << "Play -> Run: " << "WTF Mode ???" << std::endl;
		return 0;
	}

	//Game here!!!
	while (run)
    {
		if (ProcessInput(window) == 0)
		{
			run = false;
		}		
		
		DrawGraphics(window);

    }
	FinishGame();
	return 0;
}

void Game::FinishGame()
{
	std::cout << "Game -> FinishGame" << std::endl;
}

void Game::ProcessPlayerTurn()
{

}

int Game::ProcessInput(sf::RenderWindow &window)
{
	sf::Event Event;
	sf::Image screenshot;
	std::stringstream timestamp;

	PlayerAction Action = Nothing;
	//Verifying events
	while (window.pollEvent(Event))
	{
		// Window closed
		if (Event.type == sf::Event::Closed)
		{
			return 0;
		}
		//Key pressed
		if (Event.type == sf::Event::KeyPressed)
		{
			switch (Event.key.code) {
			case sf::Keyboard::Escape:
				return 0;
				break;			
			case sf::Keyboard::F1:
				timestamp << time(NULL);
				screenshot = window.capture();
				screenshot.saveToFile("screenshots/" + timestamp.str() + ".jpg");
				break;
			default:
				break;
			}

			Action = GetCurrentPlayer()->CheckAction(Event);
		}
	}

	UpdateGameLogic(Action);
	return 1;
}

void Game::DrawGraphics(sf::RenderWindow &window)
{
	window.clear();
	window.draw(displayText);
	
	Board1.BackgroundAnim.Update();
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			Board1.BackgroundAnim.getSpritePtr()->setPosition(i*50 + 100, j*50 + 400 - 250);
			window.draw(*(Board1.BackgroundAnim.getSpritePtr()));
		}
	}
	
	
	Board2.BackgroundAnim.Update();
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			Board2.BackgroundAnim.getSpritePtr()->setPosition(i*50 + 1300 - 600, j*50 + 400 - 250);
			window.draw(*(Board2.BackgroundAnim.getSpritePtr()));
		}
	}
	window.draw(Board1);
	window.draw(Board2);

	window.draw(ShotTarget);
	if (CurrentState == BoatPlacement)
	{

		sf::RectangleShape boatplace;
		boatplace.setFillColor(sf::Color(255, 0, 0, 150));
		boatplace.setPosition(ShotTarget.getPosition());
		//boatplace.setOrigin(-25, -25);
		int boatsize = BoatToPlace + 2;
		switch (BoatDirection)
		{
		case Up:
			boatplace.setSize(sf::Vector2f(50, 50 * -boatsize));
			boatplace.move(0, 50);
			break;
		case Down:
			boatplace.setSize(sf::Vector2f(50, boatsize * 50));
			break;
		case Left:
			boatplace.setSize(sf::Vector2f(50 * -boatsize, 50));	
			boatplace.move(50,0);
			break;
		case Right:
			boatplace.setSize(sf::Vector2f(50 * boatsize, 50));
			
			break;
		}
		window.draw(boatplace);
	}
	
	//Draw numbers
	sf::Text tmpText;
	
	sf::Vector2f tmpCoords;

	
	window.draw(StateText);
	tmpText.setFont(Font);
	tmpText.setCharacterSize(30);
	for (int i = 1; i <= 10; i++) {
		tmpText.setString(char(48 + i));
		if (i == 10) tmpText.setString("10");
		tmpCoords.x = Board2.Background.getPosition().x - 50 - 10;
		tmpCoords.y = Board2.Background.getPosition().y + 50 * i - 50 + 10;
		tmpText.setPosition(tmpCoords);
		window.draw(tmpText);
	}
	
	//Draw letters
	for (int i = 1; i <= 10; i++) {
		tmpText.setString(char(96 + i)); //Upper case: 64 //Lower case: 96
		tmpCoords.x = Board1.Background.getPosition().x + 50 * i - 50 + 15;
		tmpCoords.y = Board1.Background.getPosition().y - 50 + 5;
		tmpText.setPosition(tmpCoords);
		window.draw(tmpText);
	}
	//Draw letters
	for (int i = 1; i <= 10; i++) {
		tmpText.setString(char(96 + i));
		tmpCoords.x = Board2.Background.getPosition().x + 50 * i - 50 + 15;
		tmpCoords.y = Board2.Background.getPosition().y - 50 + 5;
		tmpText.setPosition(tmpCoords);
		window.draw(tmpText);
	}
	
	window.display();
}

void Game::UpdateGameLogic(PlayerAction Action)
{
	if (CurrentState == PreGame)
	{
		SetState(BoatPlacement);
	}
	else if (CurrentState == BoatPlacement)
	{

		if (Action == Place)
		{
			std::cout << "PLACINGBOAT ->";
			if (!TryPlaceBoat()) {
				displayText.setColor(sf::Color::Red);
				displayText.setString("Incorrect position. Try again...");
			} else {
				displayText.setString("");
			}
		}
		else if (Action == MoveUp)
		{
			ShotTargetCoord.Y -= 1;
			if (ShotTargetCoord.Y <= 1)
			{
				ShotTargetCoord.Y = 1;
			}
		}
		else if (Action == MoveDown)
		{
			ShotTargetCoord.Y += 1;
			if (ShotTargetCoord.Y >= 10)
			{
				ShotTargetCoord.Y = 10;
			}
		}
		else if (Action == MoveRight)
		{
			ShotTargetCoord.X += 1;
			if (ShotTargetCoord.X > 10)
			{
				ShotTargetCoord.X = 10;
			}
		}
		else if (Action == MoveLeft)
		{
			ShotTargetCoord.X -= 1;
			if (ShotTargetCoord.X < 1)
			{
				ShotTargetCoord.X = 1;
			}
		}
		else if (Action == Fire) 
		{
			BoatDirection =  Direction( BoatDirection+1 );
			if (BoatDirection == None)
			{
				BoatDirection = Up;
			}			
		}
		
	}
	else if (CurrentState == TargetSelection)
	{
		if (Action == MoveUp)
		{
			ShotTargetCoord.Y -= 1;
			if (ShotTargetCoord.Y <= 1)
			{
				ShotTargetCoord.Y = 1;
			}
		}
		else if (Action == MoveDown)
		{
			ShotTargetCoord.Y += 1;
			if (ShotTargetCoord.Y < 0)
			{
				ShotTargetCoord.Y = 0;
			}
		}
		else if (Action == MoveRight)
		{
			ShotTargetCoord.X += 1;
			if (ShotTargetCoord.X > 10)
			{
				ShotTargetCoord.X = 10;
			}
		}
		else if (Action == MoveLeft)
		{
			ShotTargetCoord.X -= 1;
			if (ShotTargetCoord.X < 1)
			{
				ShotTargetCoord.X = 1;
			}
		}
		else if (Action == Fire) // change turns for now
		{
			if (CurrentTurn == 0)
			{
			
			Board1.setShot(ShotTargetCoord);
			}
			if (CurrentTurn == 1)
			{

				Board2.setShot(ShotTargetCoord);
			}
			FinishTurn();
			
		}

		

	}
	UpdateShotTarget();
}

Player * Game::GetCurrentPlayer()
{
	if (CurrentTurn == 0)
	{
		return Player1; 
	}
	else
	{
		return Player2;
	}
}

void Game::UpdateShotTarget()
{
	GameBoard * Board = &Board1;
	if (CurrentTurn == 1)
	{
		Board = &Board2;
	}

	sf::Vector2f LocalOffset;
	LocalOffset.x = ShotTargetCoord.X * 50 -50;
	LocalOffset.y = ShotTargetCoord.Y * 50 -50;

	ShotTarget.setPosition(Board->Background.getPosition() + LocalOffset);
}

void Game::SetState(GameState newState)
{
	
	switch (newState)
	{
	case BoatPlacement:
		StateText.setString("Boat Placement");
		break;
	case PreGame:
		StateText.setString("Pre Game");
		break;
	case TargetSelection:
		StateText.setString("Target Selection");
		break;
	}

	CurrentState = newState;
}

bool Game::TryPlaceBoat()
{
	GameBoard * board = &Board1;
	if (CurrentTurn == 1)
	{
		board = &Board2;
	}
	int boatsize = BoatToPlace + 2;
	Coordinate coord = ShotTargetCoord;
	switch (BoatDirection)
	{
	case Up:
		for (int i = 0; i < boatsize; i++)
		{
			if (board->getCell(coord) != Water_Cell)
			{
				std::cout << "PLACEBOAT FAILED " << std::endl;
				return false;
			}

			coord.Y--;
		}
		break;
	case Down:
		for (int i = 0; i < boatsize; i++)
		{
			if (board->getCell(coord) != Water_Cell)
			{
				std::cout << "PLACEBOAT FAILED "<< std::endl;
				return false;
			}

			coord.Y++;
		}
		break;
	case Left:
		for (int i = 0; i < boatsize; i++)
		{
			if (board->getCell(coord) != Water_Cell)
			{
				std::cout << "PLACEBOAT FAILED "<< std::endl;
				return false;
			}

			coord.X--;
		}
		break;
	case Right:
		for (int i = 0; i < boatsize; i++)
		{
			if (board->getCell(coord) != Water_Cell)
			{
				std::cout << "PLACEBOAT FAILED "<< std::endl;
				return false;
			}

			coord.X++;
		}
		break;
	}
	std::cout << "PLACEBOAT SUCCEEED "<< std::endl;


	board->setBoat(ShotTargetCoord, BoatToPlace, BoatDirection);


	FinishTurn();
	BoatsPlaced--;
	if (BoatsPlaced <= 0)
	{
		SetState(TargetSelection);
	}

	return true;
}

void Game::FinishTurn()
{
	if (CurrentTurn == 0)
	{
		CurrentTurn = 1;
	}
	else
	{
		CurrentTurn = 0;
	}
}