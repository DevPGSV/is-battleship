#include "HumanPlayer.h"


HumanPlayer::HumanPlayer ( ) {
}

HumanPlayer::~HumanPlayer ( ) { }




PlayerAction HumanPlayer::CheckAction(sf::Event Event)
{
	if (Event.type == sf::Event::KeyPressed)
	{
		switch (Event.key.code)
		{
		case sf::Keyboard::Up:
			return MoveUp;
			break;
		case sf::Keyboard::Down:
			return MoveDown;
			break;
		case sf::Keyboard::Left:
			return MoveLeft;
			break;
		case sf::Keyboard::Right:
			return MoveRight;
			break;
		case sf::Keyboard::Space:
			return Fire;
			break;
		case sf::Keyboard::RControl:
			return Place;
			break;
		}
	}

	return Nothing;
}
