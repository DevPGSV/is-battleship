#ifndef COORDINATE_H
#define COORDINATE_H

struct Coordinate
{
	int X;
	int Y;
};

inline bool operator==(const Coordinate lhs, const Coordinate rhs)  {
	if ((lhs.X == rhs.X) && (lhs.Y == rhs.Y)) {
		return true;
	} else {
		return false;
	}
}

#endif