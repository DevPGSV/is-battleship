#include <iostream>
#include <SFML/Window/Keyboard.hpp>
#include "ThemeMenu.h"
#include "ResourceManager.h"

ThemeMenu::ThemeMenu() {
	//Preparation
	std::cout << "ThemeMenu -> Constructor" << std::endl;
	StartGame();
}

void ThemeMenu::StartGame() {
	//First time execution
	std::cout << "ThemeMenu -> StartGame" << std::endl;
	menuSelected = 0;

	Font = ResourceManager::GetInstance()->GetFont("RESOURCES/font/sansation.ttf");	

	if (Font == nullptr) {
		std::cout << "Error loading 'RESOURCES/font/sansation.ttf'" << std::endl;
        return;
    }

	title.setTexture(*ResourceManager::GetInstance()->GetTexture("RESOURCES/img/title/title.png"));
	title.setPosition(sf::Vector2f(1300/2-700/2, -30));
	
	menuEntryList.push_back(createEntry("Default", 550, 150, 0, 40, "theme", "default"));
	menuEntryList.push_back(createEntry("Pirates", 550, 250, 0, 40, "theme", "Pirates"));
	menuEntryList.push_back(createEntry("Star Wars", 550, 350, 0, 40, "theme", "StarWars"));
	menuEntryList.push_back(createEntry("Don't change", 550, 450, 0, 40, "", ""));
}

int ThemeMenu::Run(sf::RenderWindow &window, std::map<std::string, std::string> &dataStr) {
    sf::Event Event;
    bool optSelected = false;

	while (!optSelected)
    {
        //Verifying events
        while (window.pollEvent(Event))
        {
            // Window closed
            if (Event.type == sf::Event::Closed)
            {
                return (0);
            }
            //Key pressed
            if (Event.type == sf::Event::KeyPressed)
            {
                switch (Event.key.code) {
					case sf::Keyboard::Up:
						menuSelected--;
						break;
					case sf::Keyboard::Down:
						menuSelected++;
						break;
					default:
						break;
				}

				if (menuSelected < 0) menuSelected = menuEntryList.size() - 1;
				if (menuSelected > menuEntryList.size() - 1) menuSelected = 0;

				if ((Event.key.code == sf::Keyboard::Return) || (Event.key.code == sf::Keyboard::Space)){
					optSelected = true;
                }

				if (Event.key.code == sf::Keyboard::Escape) {
					return 0;
                }
            }
        }
		
		for (int i = 0; i < menuEntryList.size(); i++) {
			menuEntryList[i].text.setColor(sf::Color::White);
		}
		menuEntryList[menuSelected].text.setColor(sf::Color::Blue);
		
        //Clearing screen
        window.clear();
        //Drawing
        window.draw(title);
		for (int i = 0; i < menuEntryList.size(); i++) {
			window.draw(menuEntryList[i].text);
		}
        window.display();

    }
	dataStr[menuEntryList[menuSelected].mapKey] = menuEntryList[menuSelected].mapValue;
	return menuEntryList[menuSelected].returnValue;
}

ThemeMenu::menuEntry ThemeMenu::createEntry(std::string str, float positionX, float positionY, int returnValue, int charSize, std::string mapKey, std::string mapValue) {
	menuEntry entry;
	entry.isSelected = false;
	entry.returnValue = returnValue;
	if (Font != nullptr)
	{
		entry.text.setFont(*Font);
	}
	entry.text.setCharacterSize(charSize);
    entry.text.setString(str);
	entry.text.setPosition(positionX, positionY);
	entry.mapKey = mapKey;
	entry.mapValue = mapValue;
	return entry;
}
