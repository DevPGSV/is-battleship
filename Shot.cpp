#include "Shot.h"
#include "ResourceManager.h"
#include "SFML/Graphics/RenderTarget.hpp"

Shot::Shot ( ) {
	
}

Shot::~Shot ( ) { }

bool Shot::InitShot(ShotType Type, Coordinate Pos, sf::Vector2f boardI)
{
	sf::Texture * Tex = nullptr;

	if (Type == BoatShot)
	{
		Tex = ResourceManager::GetInstance()->GetTexture("RESOURCES/img/shot/shot.png");
	}
	else
	{
		Tex = ResourceManager::GetInstance()->GetTexture("RESOURCES/img/shot/esp/shotLB.png");
	}
	if (Tex == nullptr) {
		return false;//Error Loading the texture
	}
	sprite.setTexture(*Tex);


	shotType = Type;
	Position = Pos;
	
	coordinates.X = boardI.x + Pos.X * 50 - 50;
	coordinates.Y = boardI.y + Pos.Y * 50 - 50;
	sprite.setPosition(coordinates.X, coordinates.Y);
	return true;
}

Coordinate Shot::getPosition() {
	return Position;
}

ShotType Shot::getShotType() {
	return shotType;
}

void Shot::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	target.draw(sprite, states);
}