#pragma once
#include <SFML/Graphics.hpp>
#include <string>
#include "cScreen.h"
#include "GameBoard.h"
#include "Player.h"

enum GameState
{
	PreGame,BoatPlacement,TargetSelection
};

class Game : public cScreen{
private:
	sf::Font Font;
	void StartGame();
	void FinishGame();
	void ProcessPlayerTurn();
	int ProcessInput(sf::RenderWindow &window);
	void DrawGraphics(sf::RenderWindow &window);
	void UpdateGameLogic(PlayerAction Action);
	void UpdateShotTarget();

	bool TryPlaceBoat();
	
	void FinishTurn();
	GameBoard Board1;
	GameBoard Board2;

	GameState CurrentState;

	sf::Sprite ShotTarget;
	Coordinate ShotTargetCoord;
	Direction BoatDirection;
	BoatType BoatToPlace;
	
	Player * Player1;
	Player * Player2;
	int CurrentTurn;
	sf::Text StateText;
	sf::Text displayText;
	int BoatsPlaced;
public:

	void SetState(GameState newState);

	Player * GetCurrentPlayer();
	Game();
	virtual int Run(sf::RenderWindow &window, std::map<std::string, std::string> &dataStr);


};