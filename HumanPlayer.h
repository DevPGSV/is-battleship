
#ifndef HUMANPLAYER_H
#define HUMANPLAYER_H
#include "Player.h"

class HumanPlayer : virtual public Player
{
public:
	HumanPlayer();


	virtual ~HumanPlayer();

	virtual PlayerAction CheckAction(sf::Event Event);
};

#endif // HUMANPLAYER_H
